/*
//funcional con encrypter.js
import 'dart:convert';
import 'package:encrypt/encrypt.dart' as encrypt;

class DecryptHelper {
  static const String SECRET_KEY = "12345678901234567890123456789012"; // Clave secreta de 32 caracteres
  static const String ALGORITHM = 'aes-256-cbc';

  String decrypt(String encryptedText) {
    final parts = encryptedText.split(':');
    final iv = encrypt.IV.fromBase16(parts[0]);
    final content = parts[1];

    final key = encrypt.Key.fromUtf8(SECRET_KEY);
    final encrypter = encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

    return encrypter.decrypt(encrypt.Encrypted.fromBase16(content), iv: iv);
  }
}
*/

//mejora para encriptar el secret_key
import 'dart:convert';
import 'package:encrypt/encrypt.dart' as encrypt;

class DecryptHelper {
  static const String ENCRYPTED_SECRET_KEY = "f6cbd8e2449dfb04d2a2b22cc023f16c:6cdef92e978c7bebbd827d5864d3b335f2c1f97d743fe198964312bba2619e2b8618a763d78713d72afd7f759d146d75";  // El valor obtenido de encryptSecretKey()
  static const String ENCRYPTION_KEY = "aplicacionigobplusinovacionuitga"; // Nueva clave para desencriptar SECRET_KEY

  String getDecryptedSecretKey() {
    final parts = ENCRYPTED_SECRET_KEY.split(':');
    final iv = encrypt.IV.fromBase16(parts[0]);
    final content = parts[1];

    final key = encrypt.Key.fromUtf8(ENCRYPTION_KEY);
    final encrypter = encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

    return encrypter.decrypt(encrypt.Encrypted.fromBase16(content), iv: iv);
  }

  String decrypt(String encryptedText) {
    final decryptedSecretKey = getDecryptedSecretKey();
    
    final parts = encryptedText.split(':');
    final iv = encrypt.IV.fromBase16(parts[0]);
    final content = parts[1];

    final key = encrypt.Key.fromUtf8(decryptedSecretKey);
    final encrypter = encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

    return encrypter.decrypt(encrypt.Encrypted.fromBase16(content), iv: iv);
  }
}
