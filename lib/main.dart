import 'package:flutter/material.dart';
import 'package:igob_plus/presentation/login/splash/splash_presenter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'iGOB PLUS',
      theme: ThemeData(
        useMaterial3: true,
      ),
      home: Builder(
        builder: (context) {
          final splashPresenter = SplashPresenter();
          return splashPresenter.buildSplashScreen(context);
        },
      ),
    );
  }
}

/*
//Este archivo permanece simple y solo inicializa la aplicación.
import 'package:flutter/material.dart';
import 'package:igob_plus/core/uses_cases/fetch_data_use_case.dart';
import 'presentation/home/home_page.dart';
import 'infrastructure/data_sources/api_client.dart';
import 'infrastructure/repositories/web_service_repository.dart';

import 'presentation/home/home_presenter.dart';

void main() {
  final apiClient = ApiClient();
  final webServiceRepository = WebServiceRepository(apiClient);
  final fetchDataUseCase = FetchDataUseCase(webServiceRepository);
  final homePresenter = HomePresenter(fetchDataUseCase);

  runApp(MyApp(homePresenter));
}

class MyApp extends StatelessWidget {
  final HomePresenter homePresenter;

  MyApp(this.homePresenter);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'iGOB PLUS',
      theme: ThemeData(
        useMaterial3: true,
      ),
      home: MyHomePage(homePresenter),
    );
  }
}
*/