import 'package:flutter/material.dart';

// Input para solo números
class NumberInputField extends StatelessWidget {
  final TextEditingController controller;
  final String labelText;

  const NumberInputField({required this.controller, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(labelText: labelText),
      keyboardType: TextInputType.number,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Por favor ingrese un número';
        }
        if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
          return 'Solo se permiten números';
        }
        return null;
      },
    );
  }
}

// Input para solo letras
class LetterInputField extends StatelessWidget {
  final TextEditingController controller;
  final String labelText;

  const LetterInputField({required this.controller, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(labelText: labelText),
      keyboardType: TextInputType.text,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Por favor ingrese texto';
        }
        if (!RegExp(r'^[a-zA-Z]+$').hasMatch(value)) {
          return 'Solo se permiten letras';
        }
        return null;
      },
    );
  }
}

// Input para email
class EmailInputField extends StatelessWidget {
  final TextEditingController controller;
  final String labelText;

  const EmailInputField({required this.controller, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(labelText: labelText),
      keyboardType: TextInputType.emailAddress,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Por favor ingrese un email';
        }
        if (!RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$').hasMatch(value)) {
          return 'Por favor ingrese un email válido';
        }
        return null;
      },
    );
  }
}
TextEditingController _usuarioController = TextEditingController();


class usuarioInputField extends StatelessWidget {
  final TextEditingController controller;
  final String labelText;

  const usuarioInputField({required this.controller, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5), // Color y opacidad de la sombra
            spreadRadius: 3, // Radio de expansión
            blurRadius: 7, // Radio de difuminado
            offset: Offset(0, 3), // Desplazamiento de la sombra
          ),
        ],
      ),
      child: TextFormField(
        controller: controller,
        decoration: InputDecoration(
          labelText: "CÉDULA DE IDENTIDAD o NIT".toString(),
          hintText: 'CÉDULA DE IDENTIDAD o NIT',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          filled: true,
          fillColor: Colors.white,
          contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          suffixIcon: GestureDetector(
            child: Icon(
              Icons.account_circle_sharp,
              color: Color(0xFFD166CA),
            ),
          ),
        ),
        onChanged: (value) {
          // Aquí puedes manejar los cambios en el campo de texto
        },
      ),
    );
  }
  
}

































class usuarioInputField2 extends StatelessWidget {
  final TextEditingController controller;
  final String labelText;

  const usuarioInputField2({required this.controller, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      child: TextFormField(
      controller: _usuarioController,
      decoration: InputDecoration(
        labelText: 'Usuario'.toString(),
        hintText: 'CÉDULA DE IDENTIDAD o NIT2',
        border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        suffixIcon: GestureDetector(
          child: Icon(
            Icons.account_circle_sharp,
            color: Color(0xFFD166CA),
          ),
        ),
      ),
      onChanged: (value) {
      
      },
    ),
    );
  }
  
}













//////////////////////////////
///
class UsuarioInputField extends StatefulWidget {
  final TextEditingController controller;
  final String labelText;

  const UsuarioInputField({required this.controller, required this.labelText});

  @override
  _UsuarioInputFieldState createState() => _UsuarioInputFieldState();
}

class _UsuarioInputFieldState extends State<UsuarioInputField> {
  late FocusNode _focusNode;
  bool isFocused = false;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
    _focusNode.addListener(() {
      setState(() {
        isFocused = _focusNode.hasFocus;
      });
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        boxShadow: isFocused
            ? [
                BoxShadow(
                  color: Colors.grey.withOpacity(0),
                  spreadRadius: 3,
                  blurRadius: 7,
                  offset: Offset(0, 3),
                ),
              ]
            : [],
      ),
      child: TextFormField(
        controller: widget.controller,
        focusNode: _focusNode,
        decoration: InputDecoration(
          labelText: widget.labelText.toString(),
          hintText: 'CÉDULA DE IDENTIDAD o NIT3',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          filled: true,
          fillColor: Colors.white,
          contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          suffixIcon: GestureDetector(
            child: Icon(
              Icons.account_circle_sharp,
              color: Color(0xFFD166CA),
            ),
          ),
        ),
        onChanged: (value) {
          // Aquí puedes manejar los cambios en el campo de texto
        },
      ),
    );
  }
}
/*
//llama a los camops
// Input para solo números
class NumberInputField extends StatelessWidget {
  final TextEditingController controller;
  final String labelText;

  NumberInputField({required this.controller, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(labelText: labelText),
      keyboardType: TextInputType.number,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Por favor ingrese un número';
        }
        if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
          return 'Solo se permiten números';
        }
        return null;
      },
    );
  }
}

// Input para solo letras
class LetterInputField extends StatelessWidget {
  final TextEditingController controller;
  final String labelText;

  LetterInputField({required this.controller, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(labelText: labelText),
      keyboardType: TextInputType.text,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Por favor ingrese texto';
        }
        if (!RegExp(r'^[a-zA-Z]+$').hasMatch(value)) {
          return 'Solo se permiten letras';
        }
        return null;
      },
    );
  }
}

// Input para email
class EmailInputField extends StatelessWidget {
  final TextEditingController controller;
  final String labelText;

  EmailInputField({required this.controller, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(labelText: labelText),
      keyboardType: TextInputType.emailAddress,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Por favor ingrese un email';
        }
        if (!RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$').hasMatch(value)) {
          return 'Por favor ingrese un email válido';
        }
        return null;
      },
    );
  }
}
*/