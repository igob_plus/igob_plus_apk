//Este archivo define el caso de uso para la acción de obtener datos desde el servidor.

import '../../infrastructure/repositories/web_service_repository.dart';

class FetchDataUseCase {
  final WebServiceRepository repository;

  FetchDataUseCase(this.repository);

  Future<Map<String, dynamic>> execute(String version) {
    return repository.getVersionData(version);
  }

  //********************************* */
  //   Modulos de sigue tu tramite
  //********************************* */
  Future<Map<String, dynamic>> getAllTramites() {
    return repository.getAllTramites();
  }

  Future<Map<String, dynamic>> getAllPasosTramite(int nroTramite, String clave) {
    return repository.getAllPasosTramite(nroTramite, clave);
  }

  //********************************* */
  //   Modulos de sigue tu tramite
  //********************************* */
  Future<Map<String, dynamic>> getAllRechazos() {
    return repository.getAllRechazos();
  }

  Future<Map<String, dynamic>> getAllHospitales() {
    return repository.getAllHospitales();
  }

  Future<Map<String, dynamic>> getAllAtenciones() {
    return repository.getAllAtenciones();
  }
}
