/***** */
//encriptacion funcional de ips

import 'dart:async';

import 'package:igob_plus/config/config.dart';
import 'package:igob_plus/helpers/aes_helper.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ApiClient {
  final String baseUrl;

  ApiClient() : baseUrl = (AppConfig.spVersionador);

  Future<Map<String, dynamic>> postData(
      String endpoint, Map<String, dynamic> body) async {
    final decrypter = DecryptHelper();
    final encryptedText =
        baseUrl; // Aquí debes poner el texto cifrado que obtuviste de tu script Node.js
    final decryptedText = decrypter.decrypt(encryptedText);

    print("Texto desencriptado: $decryptedText");

    final response = await http.post(
      Uri.parse('$decryptedText$endpoint'),
      body: jsonEncode(body),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Error al conectar al servicio web');
    }
  }

  /**************************************************** */
  //          Peticiones de sigue tu tramite
  /**************************************************** */
  Future<Map<String, dynamic>> getAllTramites() async {
    final ci = '91184'; //8781'; // Reemplaza con tu valor para ci

    final response = await http.post(
      Uri.parse('https://gamlpmotores.lapaz.bo/gamlp/wsSTTF/lstTramitesSITRAM'),
      headers: <String, String>{
        'Accept': '*/*',
        'Accept-Language': 'es-ES,es;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://igob247.lapaz.bo',
        'Referer': 'https://igob247.lapaz.bo/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36',
        'sec-ch-ua':
            '"Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
      },
      body: {
        'ci': ci,
      },
    ).timeout(
      const Duration(seconds: 20),
      onTimeout: () {
        print('Timeout during HTTP request');
        return http.Response('Tiempo Limite exedido', 500);
      },
    );
    //print(response);

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print('Error al conectar al servicio web');
      throw Exception('Error al conectar al servicio web');
    }
  }

  Future<Map<String, dynamic>> getAllPasosTramite(
      int nroTramite, String clave) async {
    final response = await http.post(
      Uri.parse(
          'https://gamlpmotores.lapaz.bo/gamlp/wsSTTF/visualizarTramitesSITRAM'),
      headers: <String, String>{
        'Accept': '*/*',
        'Accept-Language': 'es-ES,es;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://igob247.lapaz.bo',
        'Referer': 'https://igob247.lapaz.bo/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36',
        'sec-ch-ua':
            '"Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
      },
      body: {
        'nroTramite': nroTramite.toString(),
        'clave': clave.toString(),
      },
    ) /*.timeout(
        const Duration(seconds: 20),
        onTimeout: () {
          throw TimeoutException('Timeout during HTTP request');
        },
      )*/
        ;
    if (response.statusCode == 200) {
      //print('Si');
      return jsonDecode(response.body);
    } else {
      //print('No');
      throw Exception('Error al conectar al servicio web');
    }
  }

  /**************************************************** */
  //          CITAS MEDICAS
  /**************************************************** */

  Future<Map<String, dynamic>> getAllRechazos() async {
    const query =
        "SELECT * from sp_listar_hojas_referencia_rechazadas(\$\$8326875\$\$)";
    Map<String, String> body = {"consulta": query};
    final response = await http.post(
      Uri.parse('https://gamlpmotores.lapaz.bo/salud/wsSalud/dinamico'),
      headers: <String, String>{
        'Accept': '*/*',
        'Accept-Language': 'es-ES,es;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/json',
        'Origin': 'https://igob247.lapaz.bo',
        'Referer': 'https://igob247.lapaz.bo/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36',
        'sec-ch-ua':
            '"Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
      },
      body: jsonEncode(body),
    ) /*.timeout(
        const Duration(seconds: 20),
        onTimeout: () {
          throw TimeoutException('Timeout during HTTP request');
        },
      )*/
        ;
    //print(response);
    if (response.statusCode == 200) {
      //print('Si');
      return jsonDecode(response.body);
    } else {
      //print('No');
      throw Exception('Error al conectar al servicio web');
    }
  }

  Future<Map<String, dynamic>> getAllHospitales() async {
    final Map<String, dynamic> data = {
      "ci": "6087477",
      // Otros campos que necesites enviar
    };

    final response = await http.post(
      Uri.parse(
          'https://gamlpmotores.lapaz.bo/salud/wsSalud/listaHospitalIgob'),
      headers: <String, String>{
        'Accept': '*/*',
        'Accept-Language': 'es-ES,es;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/json',
        'Origin': 'https://igob247.lapaz.bo',
        'Referer': 'https://igob247.lapaz.bo/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36',
        'sec-ch-ua':
            '"Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
      },
      body: jsonEncode(data),
    ) /*.timeout(
        const Duration(seconds: 20),
        onTimeout: () {
          throw TimeoutException('Timeout during HTTP request');
        },
      )*/
        ;
    //print(response);
    if (response.statusCode == 200) {
      //print('Si2');
      return jsonDecode(response.body);
    } else {
      //print('No2');
      throw Exception('Error al conectar al servicio web');
    }
  }

  Future<Map<String, dynamic>> getAllAtenciones() async {
    final Map<String, dynamic> data = {
      "ci": "8326875",//8326875
      // Otros campos que necesites enviar
    };

    final response = await http.post(
      Uri.parse(
          'https://gamlpmotores.lapaz.bo/salud/wsSalud/listaAtencion'),
      headers: <String, String>{
        'Accept': '*/*',
        'Accept-Language': 'es-ES,es;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/json',
        'Origin': 'https://igob247.lapaz.bo',
        'Referer': 'https://igob247.lapaz.bo/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36',
        'sec-ch-ua':
            '"Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
      },
      body: jsonEncode(data),
    ) /*.timeout(
        const Duration(seconds: 20),
        onTimeout: () {
          throw TimeoutException('Timeout during HTTP request');
        },
      )*/
        ;
    //print(response);
    if (response.statusCode == 200) {
      //print('Si2');
      return jsonDecode(response.body);
    } else {
      //print('No2');
      throw Exception('Error al conectar al servicio web');
    }
  }
}
