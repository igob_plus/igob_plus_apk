import 'dart:convert';
import 'package:intl/intl.dart';

List<Tramite> allTramitesFromJson(List<dynamic> parsedJson) {
  List<Tramite> tramites = [];
  for (var item in parsedJson) {
    //print('*****************');
    tramites.add(Tramite.fromJson(item));
    //print(tramites.last.toString());
  }
  return tramites;
}

List<Tramite> ordenarTrmitePorFecha(List<Tramite> tramites) {
  tramites.sort((a, b) {
    DateTime fechaA = DateFormat('dd/MM/yyyy').parse(a.tfregistro);
    DateTime fechaB = DateFormat('dd/MM/yyyy').parse(b.tfregistro);
    return fechaB.compareTo(fechaA);
  });
  return tramites;
}

String allTramitesToJson(List<Tramite> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Tramite {
  String toid;
  Usuario tdata;
  int tcorrelativo;
  String tasunto;
  String tfregistro;

  Tramite({
    required this.toid,
    required this.tdata,
    required this.tcorrelativo,
    required this.tasunto,
    required this.tfregistro,
  });

  factory Tramite.fromJson(Map<String, dynamic> json) => Tramite(
        toid: json["toid"],
        tdata: Usuario.fromJson(jsonDecode(json['tdata'])),
        tcorrelativo: json["tcorrelativo"],
        tasunto: json["tasunto"],
        tfregistro:
            DateFormat('dd/MM/yyyy').format(DateTime.parse(json["tfregistro"])),
      );

  Map<String, dynamic> toJson() => {
        "toid": toid,
        "tdata": tdata,
        "tcorrelativo": tcorrelativo,
        "tasunto": tasunto,
        "tfregistro": tfregistro,
      };
}

class Usuario {
  /*String asunto;
  String solicitante;
  int region;
  String INT_SOLICITANTE;
  String direccion;
  String INT_CORREO;
  String INT_TEL_CELULAR;
  String telefonoFijo;
  String RC_NOMBRE;
  String RC_PATERNO;
  String RC_MATERNO;
  String nombreRazonSocial;
  String direccionJuridico;
  String RC_CORREO;
  String telefonoFijoJuridico;
  String INT_RL_NUM_DOCUMENTOj;
  String INT_RL_NUM_DOCUMENTO;*/
  String tipo_tramite;
  String tipo_tramite_id;

  Usuario({
    /*required this.asunto,
    required this.solicitante,
    required this.region,
    required this.INT_SOLICITANTE,
    required this.direccion,
    required this.INT_CORREO,
    required this.INT_TEL_CELULAR,
    required this.telefonoFijo,
    required this.RC_NOMBRE,
    required this.RC_PATERNO,
    required this.RC_MATERNO,
    required this.nombreRazonSocial,
    required this.direccionJuridico,
    required this.RC_CORREO,
    required this.telefonoFijoJuridico,
    required this.INT_RL_NUM_DOCUMENTOj,
    required this.INT_RL_NUM_DOCUMENTO,*/
    required this.tipo_tramite,
    required this.tipo_tramite_id,
  });

  factory Usuario.fromJson(Map<String, dynamic> json) {
    return Usuario(
      /*asunto: json["asunto"] ?? "", // Valor predeterminado si es nulo
    solicitante: json["solicitante"] ?? "", // Valor predeterminado si es nulo
    region: json["region"] ?? 0, // Valor predeterminado si es nulo
    INT_SOLICITANTE: json["INT_SOLICITANTE"] ?? "", // Valor predeterminado si es nulo
    direccion: json["direccion"] ?? "", // Valor predeterminado si es nulo
    INT_CORREO: json["INT_CORREO"] ?? "", // Valor predeterminado si es nulo
    INT_TEL_CELULAR: json["INT_TEL_CELULAR"] ?? "", // Valor predeterminado si es nulo
    telefonoFijo: json["telefonoFijo"] ?? "", // Valor predeterminado si es nulo
    RC_NOMBRE: json["RC_NOMBRE"] ?? "", // Valor predeterminado si es nulo
    RC_PATERNO: json["RC_PATERNO"] ?? "", // Valor predeterminado si es nulo
    RC_MATERNO: json["RC_MATERNO"] ?? "", // Valor predeterminado si es nulo
    nombreRazonSocial: json["nombreRazonSocial"] ?? "", // Valor predeterminado si es nulo
    direccionJuridico: json["direccionJuridico"] ?? "", // Valor predeterminado si es nulo
    RC_CORREO: json["RC_CORREO"] ?? "", // Valor predeterminado si es nulo
    telefonoFijoJuridico: json["telefonoFijoJuridico"] ?? "", // Valor predeterminado si es nulo
    INT_RL_NUM_DOCUMENTOj: json["INT_RL_NUM_DOCUMENTOj"] ?? "", // Valor predeterminado si es nulo
    INT_RL_NUM_DOCUMENTO: json["INT_RL_NUM_DOCUMENTO"] ?? "", // Valor predeterminado si es nulo*/
      tipo_tramite:
          json["tipo_tramite"] ?? "", // Valor predeterminado si es nulo
      tipo_tramite_id:
          json["tipo_tramite_id"] ?? "", // Valor predeterminado si es nulo
    );
  }

  Map<String, dynamic> toJson() => {
        /*"asunto": asunto,
        "solicitante": solicitante,
        "region": region,
        "INT_SOLICITANTE": INT_SOLICITANTE,
        "direccion": direccion,
        "INT_CORREO": INT_CORREO,
        "INT_TEL_CELULAR": INT_TEL_CELULAR,
        "telefonoFijo": telefonoFijo,
        "RC_NOMBRE": RC_NOMBRE,
        "RC_PATERNO": RC_PATERNO,
        "RC_MATERNO": RC_MATERNO,
        "nombreRazonSocial": nombreRazonSocial,
        "direccionJuridico": direccionJuridico,
        "RC_CORREO": RC_CORREO,
        "telefonoFijoJuridico": telefonoFijoJuridico,
        "INT_RL_NUM_DOCUMENTOj": INT_RL_NUM_DOCUMENTOj,
        "INT_RL_NUM_DOCUMENTO": INT_RL_NUM_DOCUMENTO,*/
        "tipo_tramite": tipo_tramite,
        "tipo_tramite_id": tipo_tramite_id,
      };
}
