import 'dart:convert';
import 'package:intl/intl.dart';

List<PasoTramite> allPasosTramiteFromJson(List<dynamic> parsedJson) {
  List<PasoTramite> pasosTramite = [];
  for (var item in parsedJson) {
    //print(item);
    pasosTramite.add(PasoTramite.fromJson(item));
  }
  return pasosTramite;
}

String allPasosTramiteToJson(List<PasoTramite> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PasoTramite {
  String tno;
  String trd;

  PasoTramite({
    required this.tno,
    required this.trd,
  });

  factory PasoTramite.fromJson(Map<String, dynamic> json) => PasoTramite(
        tno: json["tno"],
        trd: json["trd"],
      );

  Map<String, dynamic> toJson() => {
        "tno": tno,
        "trd": trd,
      };
}
