import 'dart:convert';
import 'package:intl/intl.dart';

List<Rechazo> allRechazosFromJson(List<dynamic> parsedJson) {
  List<Rechazo> Rechazos = [];
  for (var item in parsedJson) {
   
    Rechazos.add(Rechazo.fromJson(item));
   
  }
  return Rechazos;
}

/*List<Rechazo> ordenarPorFecha(List<Rechazo> Rechazos) {
  Rechazos.sort((a, b) {
    DateTime fechaA = DateFormat('dd/MM/yyyy').parse(a.FECHA_EMISION);
    DateTime fechaB = DateFormat('dd/MM/yyyy').parse(b.FECHA_EMISION);
    return fechaB.compareTo(fechaA);
  });
  return Rechazos;
}*/

String allRechazosToJson(List<Rechazo> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Rechazo {
  String vhospital;
  DatosReferencia vhjr_datos_referencia;

  String vhjr_motivo;


  Rechazo({
    required this.vhospital,
    required this.vhjr_datos_referencia,

    required this.vhjr_motivo,

  });

  factory Rechazo.fromJson(Map<String, dynamic> json) {

  return Rechazo(
    vhjr_datos_referencia: DatosReferencia.fromJson(json['vhjr_datos_referencia']),
    vhospital: json['vhospital'],
    vhjr_motivo: json['vhjr_motivo'],
  );
}

  Map<String, dynamic> toJson() => {
        "vhjr_datos_referencia": vhjr_datos_referencia,
        "vhospital": vhospital,
        "vhjr_motivo": vhjr_motivo,

      };
}


class DatosReferencia {

  String URL_HOJA;
  String FECHA_EMISION;

  DatosReferencia({
    required this.URL_HOJA,
    required this.FECHA_EMISION
  });

  factory DatosReferencia.fromJson(Map<String, dynamic> json) {
    return DatosReferencia(
      URL_HOJA: json['URL_HOJA'],
      FECHA_EMISION: json['FECHA_EMISION'],
    );
  }
}