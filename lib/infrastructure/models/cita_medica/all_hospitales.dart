import 'dart:convert';
import 'package:intl/intl.dart';

List<Hospital> allHospitalesFromJson(List<dynamic> parsedJson) {
  List<Hospital> Hospitales = [];
  for (var item in parsedJson) {
    Hospitales.add(Hospital.fromJson(item));
  }
  return Hospitales;
}

String allHospitalesToJson(List<Hospital> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Hospital {
  int vhsp_id_hospital;
  String vhsp_nombre_hospital;
  String vhsp_direccion;
  String vhsp_telefono;
  String vhsp_descripcion;
  int vexiste;
  Hospital({
    required this.vhsp_id_hospital,
    required this.vhsp_nombre_hospital,
    required this.vhsp_direccion,
    required this.vhsp_telefono,
    required this.vhsp_descripcion,
    required this.vexiste,
  });

  factory Hospital.fromJson(Map<String, dynamic> json) {
    return Hospital(
      vhsp_id_hospital: json['vhsp_id_hospital'],
      vhsp_nombre_hospital: json['vhsp_nombre_hospital'],
      vhsp_direccion: json['vhsp_direccion'],
      vhsp_telefono: json['vhsp_telefono'],
      vhsp_descripcion: json['vhsp_descripcion'],
      vexiste: json['vexiste'],
    );
  }

  Map<String, dynamic> toJson() => {
        "vhsp_id_hospital": vhsp_id_hospital,
        "vhsp_nombre_hospital": vhsp_nombre_hospital,
        "vhsp_direccion": vhsp_direccion,
        "vhsp_telefono": vhsp_telefono,
        "vhsp_descripcion": vhsp_descripcion,
        "vexiste": vexiste,
      };
}
