import 'dart:convert';

List<Atencion> allAtencionesFromJson(List<dynamic> parsedJson) {
  List<Atencion> Atenciones = [];
  for (var item in parsedJson) {
    Atenciones.add(Atencion.fromJson(item));
  }
  return Atenciones;
}

String allAtencionesToJson(List<Atencion> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
class Atencion {
 int presid;
  int presdtspsl_id;
  String presestado_prestacion;
  int presnumero_ficha;
  String prescodigo_ficha;
  String presfecha_atencion;
  String preshora_inicio_ficha;
  String preshora_fin_ficha;
  String cnsldescripcion;
  String paciente_nombre;
  String trndescripcion;
  int espid;
  String espdesc_especialidad;
  int presturno_id;
  String preshabilitacion;
  int presidhospital;
  String vhsp_nombre_hospital;
  String vhcl_codigoseg;
  int vusr_id;
  String vesp_cod_especialidad;
  String vpres_revertir;
  String vficha_pagada;
  String vfact_url_factura;
  int vitm_sucursal;
  int vitm_cod_ur;
  double vitm_monto;
  int vitm_cod_item;
  int vsucrl_idusuario;
  String vsucrl_nomb_usuario;
  String vsucrl_contr_usuario;
  String vnombres;
  String vapellidos;
  String vhsp_direccion;
  String vhsp_telefono;
  String vdtspsl_correo;
  String vdtspsl_paterno;
  String vdtspsl_ci;
  String vfecha;
  String vanio;
  String vactividad;
  String vnombre_supervisor;

  Atencion({
    required this.presid,
    required this.presdtspsl_id,
    required this.presestado_prestacion,
    required this.presnumero_ficha,
    required this.prescodigo_ficha,
    required this.presfecha_atencion,
    required this.preshora_inicio_ficha,
    required this.preshora_fin_ficha,
    required this.cnsldescripcion,
    required this.paciente_nombre,
    required this.trndescripcion,
    required this.espid,
    required this.espdesc_especialidad,
    required this.presturno_id,
    required this.preshabilitacion,
    required this.presidhospital,
    required this.vhsp_nombre_hospital,
    required this.vhcl_codigoseg,
    required this.vusr_id,
    required this.vesp_cod_especialidad,
    required this.vpres_revertir,
    required this.vficha_pagada,
    required this.vfact_url_factura,
    required this.vitm_sucursal,
    required this.vitm_cod_ur,
    required this.vitm_monto,
    required this.vitm_cod_item,
    required this.vsucrl_idusuario,
    required this.vsucrl_nomb_usuario,
    required this.vsucrl_contr_usuario,
    required this.vnombres,
    required this.vapellidos,
    required this.vhsp_direccion,
    required this.vhsp_telefono,
    required this.vdtspsl_correo,
    required this.vdtspsl_paterno,
    required this.vdtspsl_ci,
    required this.vfecha,
    required this.vanio,
    required this.vactividad,
    required this.vnombre_supervisor,
  });

  factory Atencion.fromJson(Map<String, dynamic> json) {
    return Atencion(

      vhsp_direccion: json['vhsp_direccion'],
      vhsp_telefono: json['vhsp_telefono'],
      presid: json['presid'],
      presdtspsl_id: json['presdtspsl_id'],
      presestado_prestacion: json['presestado_prestacion'],
      presnumero_ficha: json['presnumero_ficha'],
      prescodigo_ficha: json['prescodigo_ficha'],
      presfecha_atencion: json['presfecha_atencion'],
      preshora_inicio_ficha: json['preshora_inicio_ficha'],
      preshora_fin_ficha: json['preshora_fin_ficha'],
      cnsldescripcion: json['cnsldescripcion'],
      paciente_nombre: json['paciente_nombre'],
      trndescripcion: json['trndescripcion'],
      espid: json['espid'],
      espdesc_especialidad: json['espdesc_especialidad'],
      presturno_id: json['presturno_id'],
      preshabilitacion: json['preshabilitacion'],
      presidhospital: json['presidhospital'],
      vhsp_nombre_hospital: json['vhsp_nombre_hospital'],
      vhcl_codigoseg: json['vhcl_codigoseg'],
      vusr_id: json['vusr_id'],
      vesp_cod_especialidad: json['vesp_cod_especialidad'],
      vpres_revertir: json['vpres_revertir'],
      vficha_pagada: json['vficha_pagada'],
      vfact_url_factura: json['vfact_url_factura'],
      vitm_sucursal: json['vitm_sucursal'],
      vitm_cod_ur: json['vitm_cod_ur'],
      vitm_monto: double.parse(json['vitm_monto']),
      vitm_cod_item: json['vitm_cod_item'],
      vsucrl_idusuario: json['vsucrl_idusuario'],
      vsucrl_nomb_usuario: json['vsucrl_nomb_usuario'],
      vsucrl_contr_usuario: json['vsucrl_contr_usuario'],
      vnombres: json['vnombres'],
      vapellidos: json['vapellidos'],
      vdtspsl_correo: json['vdtspsl_correo'],
      vdtspsl_paterno: json['vdtspsl_paterno'],
      vdtspsl_ci: json['vdtspsl_ci'],
      vfecha: json['vfecha'],
      vanio: json['vanio'],
      vactividad: json['vactividad'],
      vnombre_supervisor: json['vnombre_supervisor'],
    );
  }

  Map<String, dynamic> toJson() => {
        "vhsp_direccion": vhsp_direccion,
        "vhsp_telefono": vhsp_telefono,
        "presid": presid,
        "presdtspsl_id": presdtspsl_id,
        "presestado_prestacion": presestado_prestacion,
        "presnumero_ficha": presnumero_ficha,
        "prescodigo_ficha": prescodigo_ficha,
        "presfecha_atencion": presfecha_atencion,
        "preshora_inicio_ficha": preshora_inicio_ficha,
        "preshora_fin_ficha": preshora_fin_ficha,
        "cnsldescripcion": cnsldescripcion,
        "paciente_nombre": paciente_nombre,
        "trndescripcion": trndescripcion,
        "espid": espid,
        "espdesc_especialidad": espdesc_especialidad,
        "presturno_id": presturno_id,
        "preshabilitacion": preshabilitacion,
        "presidhospital": presidhospital,
        "vhsp_nombre_hospital": vhsp_nombre_hospital,
        "vhcl_codigoseg": vhcl_codigoseg,
        "vusr_id": vusr_id,
        "vesp_cod_especialidad": vesp_cod_especialidad,
        "vpres_revertir": vpres_revertir,
        "vficha_pagada": vficha_pagada,
        "vfact_url_factura": vfact_url_factura,
        "vitm_sucursal": vitm_sucursal,
        "vitm_cod_ur": vitm_cod_ur,
        "vitm_monto": vitm_monto,
        "vitm_cod_item": vitm_cod_item,
        "vsucrl_idusuario": vsucrl_idusuario,
        "vsucrl_nomb_usuario": vsucrl_nomb_usuario,
        "vsucrl_contr_usuario": vsucrl_contr_usuario,
        "vnombres": vnombres,
        "vapellidos": vapellidos,
        "vdtspsl_correo": vdtspsl_correo,
        "vdtspsl_paterno": vdtspsl_paterno,
        "vdtspsl_ci": vdtspsl_ci,
        "vfecha": vfecha,
        "vanio": vanio,
        "vactividad": vactividad,
        "vnombre_supervisor": vnombre_supervisor,
      };
}
