// Este archivo define el repositorio que el caso de uso utilizará.

import '../data_sources/api_client.dart';

class WebServiceRepository {
  final ApiClient apiClient;

  WebServiceRepository(this.apiClient);

  Future<Map<String, dynamic>> getVersionData(String version) {
    return apiClient.postData('/wsRCPG/spVersionador', {'version': version});
  }

  // SITRAM
  Future<Map<String, dynamic>> getAllTramites() {
    return apiClient.getAllTramites();
  }

  Future<Map<String, dynamic>> getAllPasosTramite(int nroTramite, String clave) {
    print('getAllPasosTramite3');
    return apiClient.getAllPasosTramite(nroTramite, clave);
  }

  // CITAS MEDICAS
  Future<Map<String, dynamic>> getAllRechazos() {
    return apiClient.getAllRechazos();
  }

  Future<Map<String, dynamic>> getAllHospitales() {
    return apiClient.getAllHospitales();
  }

  Future<Map<String, dynamic>> getAllAtenciones() {
    return apiClient.getAllAtenciones();
  }
}
