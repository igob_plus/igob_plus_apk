import 'package:flutter/material.dart';

class CuentaPage extends StatefulWidget {
  @override
  _CuentaPageState createState() => _CuentaPageState();
}

class _CuentaPageState extends State<CuentaPage> {
  final _formKey = GlobalKey<FormState>();
  final List<Map<String, dynamic>> departamentos = [
{"dep_id":"LPZ", "dep_nombre": "LA PAZ"},
{"dep_id":"CHQ", "dep_nombre": "CHUQUISACA"},
{"dep_id":"CBB", "dep_nombre": "COCHABAMBA"},
{"dep_id":"BNI", "dep_nombre": "BENI"},
{"dep_id":"PND", "dep_nombre": "PANDO"},
{"dep_id":"SCZ", "dep_nombre": "SANTA CRUZ"},
{"dep_id":"TJA", "dep_nombre": "TARIJA"},
{"dep_id":"ORU", "dep_nombre": "ORURO"},
{"dep_id":"PTS", "dep_nombre": "POTOSÍ"},
  ];
  final List<Map<String, dynamic>> estado_civil = [
    {"est_id": 'C', "est_descr": "Casado/a"},
    {"est_id": 'D', "est_descr": "Divorciado/a"},
    {"est_id": 'S', "est_descr": "Soltero/a"},
    {"est_id": 'V', "est_descr": "Viudo/a"},
  ];
  final List<Map<String, dynamic>> profesiones = [
    {"prf_id": 56, "prf_profesion": "ABOGADO"},
    {"prf_id": 55, "prf_profesion": "ADMINISTRADOR DE EMPRESAS"},
    {"prf_id": 58, "prf_profesion": "AGRICULTOR"},
    {"prf_id": 57, "prf_profesion": "ALBAÑIL"},
    {"prf_id": 59, "prf_profesion": "AUXILIAR DE ENFERMERIA"},
    {"prf_id": 31, "prf_profesion": "CAJERO(A)"},
    {"prf_id": 53, "prf_profesion": "CHOFER"},
    {"prf_id": 45, "prf_profesion": "COCINERO"},
    {"prf_id": 7, "prf_profesion": "COMERCIANTE"},
    {"prf_id": 25, "prf_profesion": "COMUNICADOR(A) SOCIAL"},
    {"prf_id": 33, "prf_profesion": "CONSTRUCTOR"},
    {"prf_id": 10, "prf_profesion": "CONTADOR"},
    {"prf_id": 46, "prf_profesion": "COSMETOLOGA"},
    {"prf_id": 9, "prf_profesion": "COSTURERO(A)"},
    {"prf_id": 49, "prf_profesion": "DENTISTA"},
    {"prf_id": 38, "prf_profesion": "DOCENTE"},
    {"prf_id": 48, "prf_profesion": "ECONOMISTA"},
    {"prf_id": 37, "prf_profesion": "EDUCADORA DE NIÑOS"},
    {"prf_id": 6, "prf_profesion": "EMPLEADA"},
    {"prf_id": 14, "prf_profesion": "ENFERMERA"},
    {"prf_id": 12, "prf_profesion": "ESTILISTA"},
    {"prf_id": 4, "prf_profesion": "ESTUDIANTE"},
    {"prf_id": 35, "prf_profesion": "FABRIL"},
    {"prf_id": 24, "prf_profesion": "FISIOTERAPEUTA"},
    {"prf_id": 39, "prf_profesion": "GUIA DE TURISMO"},
    {"prf_id": 36, "prf_profesion": "INDEPENDIENTE"},
    {"prf_id": 16, "prf_profesion": "INGENIERO(A) CIVIL"},
    {"prf_id": 51, "prf_profesion": "INGENIERO(A) COMERCIAL"},
    {"prf_id": 54, "prf_profesion": "INGENIERO(A) DE SISTEMAS"},
    {"prf_id": 41, "prf_profesion": "JUBILADO(A)"},
    {"prf_id": 50, "prf_profesion": "LABORATORISTA"},
    {"prf_id": 3, "prf_profesion": "LABORES DE CASA"},
    {"prf_id": 18, "prf_profesion": "LAVANDERA"},
    {"prf_id": 27, "prf_profesion": "LICENCIADA"},
    {"prf_id": 30, "prf_profesion": "MECANICO AUTOMOTRIZ"},
    {"prf_id": 22, "prf_profesion": "MECANICO DENTAL"},
    {"prf_id": 23, "prf_profesion": "MEDICO"},
    {"prf_id": 19, "prf_profesion": "MODISTA"},
    {"prf_id": 42, "prf_profesion": "NIÑERA"},
    {"prf_id": 28, "prf_profesion": "ODONTOLOGO"},
    {"prf_id": 60, "prf_profesion": "OTROS"},
    {"prf_id": 8, "prf_profesion": "PEINADORA"},
    {"prf_id": 13, "prf_profesion": "PROFESOR(A)"},
    {"prf_id": 32, "prf_profesion": "PROMOTOR(A)"},
    {"prf_id": 40, "prf_profesion": "RECIEN NACIDO"},
    {"prf_id": 5, "prf_profesion": "RELIGIOSA"},
    {"prf_id": 29, "prf_profesion": "REPORTERO(A)"},
    {"prf_id": 11, "prf_profesion": "SECRETARIA"},
    {"prf_id": 1, "prf_profesion": "SIN ESPECIFICAR"},
    {"prf_id": 43, "prf_profesion": "TRABAJADOR MANUAL"},
    {"prf_id": 20, "prf_profesion": "TRABAJADOR(A) SOCIAL"},
    {"prf_id": 15, "prf_profesion": "TRABAJADORA DEL HOGAR"},
    {"prf_id": 34, "prf_profesion": "TRAMITADOR(A)"},
    {"prf_id": 26, "prf_profesion": "TURISTA"},
    {"prf_id": 2, "prf_profesion": "UNIVERSITARIO(A)"},
    {"prf_id": 17, "prf_profesion": "VENDEDOR(A)"},
    {"prf_id": 44, "prf_profesion": "VETERINARIO(A)"}
  ];


  String? _selectedDepartment;
  String? _selectedProfesion;
  String? _selectedEstadoCivil;
  String? _selectedGender;

  DateTime? _selectedDate;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != _selectedDate) {
      setState(() {
        _selectedDate = picked;
      });
    }
  }

  String _formatDate(DateTime? date) {
    if (date != null) {
      return '${date.day.toString().padLeft(2, '0')}-${date.month.toString().padLeft(2, '0')}-${date.year}';
    }
    return 'Selecciona la fecha';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Cuenta')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  'Nombre:',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                Text(
                  'Juan Pérez Marca Marca',
                  style: TextStyle(fontSize: 16.0),
                ),
                SizedBox(height: 16.0),
                Text(
                  'Cédula de Identidad:',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                Text(
                  '1234567 LP',
                  style: TextStyle(fontSize: 16.0),
                ),
                SizedBox(height: 16.0),
                /**************************** */
                //     FECHA DE NACIMIENTO
                Row(
                  children: <Widget>[
                    Text(
                      'Fecha de nacimiento:',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16.0),
                    ),
                    SizedBox(width: 16.0),
                    ElevatedButton(
                      onPressed: () {
                        _selectDate(context);
                      },
                      child: Text(_selectedDate != null
                          ? _formatDate(_selectedDate)
                          : 'Selecciona la fecha'),
                    ),
                  ],
                ),
                /**************************** */
                SizedBox(height: 16.0),
                DropdownButtonFormField<String>(
          items: departamentos.map((dept) {
            return DropdownMenuItem<String>(
              value: dept['dep_id'].toString(),
              child: Text(dept['dep_nombre'].toString()),
            );
          }).toList(),
          decoration: InputDecoration(
            labelText: 'Selecciona tu departamento',
            border: OutlineInputBorder(),
          ),
          value: _selectedDepartment,
          onChanged: (String? newValue) {
            setState(() {
              _selectedDepartment = newValue;
            });
          },
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Por favor, selecciona un departamento';
            }
            return null;
          },
        ),
                SizedBox(height: 16.0),
                Row(
                  children: <Widget>[
                    Text(
                      'Género:',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16.0),
                    ),
                    SizedBox(width: 16.0),
                    Radio<String>(
                      value: 'Masculino',
                      groupValue: _selectedGender,
                      onChanged: (value) {
                        setState(() {
                          _selectedGender = value;
                        });
                      },
                    ),
                    Text('Masculino'),
                    Radio<String>(
                      value: 'Femenino',
                      groupValue: _selectedGender,
                      onChanged: (value) {
                        setState(() {
                          _selectedGender = value;
                        });
                      },
                    ),
                    Text('Femenino'),
                    // Puedes agregar más radios aquí si es necesario
                  ],
                ),
                SizedBox(height: 16.0),
                DropdownButtonFormField<String>(
                  items: estado_civil.map((profesion) {
                    return DropdownMenuItem<String>(
                      value: profesion['est_id'].toString(),
                      child: Text(profesion['est_descr'].toString()),
                    );
                  }).toList(),
                  decoration: InputDecoration(
                    labelText: 'Estado Civil',
                    border: OutlineInputBorder(),
                  ),
                  value: _selectedEstadoCivil,
                  onChanged: (String? newValue) {
                    setState(() {
                      _selectedEstadoCivil = newValue;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor, selecciona tu estado civil';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                DropdownButtonFormField<String>(
                  items: profesiones.map((profesion) {
                    return DropdownMenuItem<String>(
                      value: profesion['prf_profesion'].toString(),
                      child: Text(profesion['prf_profesion'].toString()),
                    );
                  }).toList(),
                  decoration: InputDecoration(
                    labelText: 'Profesión',
                    border: OutlineInputBorder(),
                  ),
                  value: _selectedProfesion,
                  onChanged: (String? newValue) {
                    setState(() {
                      _selectedProfesion = newValue;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor, selecciona una profesión';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Telefono', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Celular', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Correo', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Direccion', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      // Si todos los campos son válidos, puedes realizar alguna acción
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text('Datos válidos')),
                      );
                    }
                  },
                  child: Text('Guardar'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
