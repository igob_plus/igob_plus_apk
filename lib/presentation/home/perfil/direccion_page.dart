import 'package:flutter/material.dart';

class DireccionPage extends StatefulWidget {
  @override
  _DireccionPageState createState() => _DireccionPageState();
}

class _DireccionPageState extends State<DireccionPage> {
  final _formKey = GlobalKey<FormState>();
  final List<Map<String, dynamic>> departamentos = [
{"dep_id":"LPZ", "dep_nombre": "LA PAZ"},
{"dep_id":"CHQ", "dep_nombre": "CHUQUISACA"},
{"dep_id":"CBB", "dep_nombre": "COCHABAMBA"},
{"dep_id":"BNI", "dep_nombre": "BENI"},
{"dep_id":"PND", "dep_nombre": "PANDO"},
{"dep_id":"SCZ", "dep_nombre": "SANTA CRUZ"},
{"dep_id":"TJA", "dep_nombre": "TARIJA"},
{"dep_id":"ORU", "dep_nombre": "ORURO"},
{"dep_id":"PTS", "dep_nombre": "POTOSÍ"},
  ];

  final List<Map<String, dynamic>> vias = [
    {"via_id": "AV", "via_nombre": "AVENIDA"},
    {"via_id": "CA", "via_nombre": "CALLE"},
    {"via_id": "CL", "via_nombre": "CALLEJON"},
    {"via_id": "PL", "via_nombre": "PLAZA"},
    {"via_id": "CN", "via_nombre": "CANCHA"},
    {"via_id": "PR", "via_nombre": "PARQUE"},
    {"via_id": "PA", "via_nombre": "PASAJE"},
    {"via_id": "ND", "via_nombre": "NO DEFINIDO"},
  ];
  String? _selectedDepartment;
  String? _selectedVia;
  DateTime? _selectedDate;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != _selectedDate) {
      setState(() {
        _selectedDate = picked;
      });
    }
  }

  String _formatDate(DateTime? date) {
    if (date != null) {
      return '${date.day.toString().padLeft(2, '0')}-${date.month.toString().padLeft(2, '0')}-${date.year}';
    }
    return 'Selecciona la fecha';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Direccion')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                /**************************** */
                SizedBox(height: 16.0),
                DropdownButtonFormField<String>(
          items: departamentos.map((dept) {
            return DropdownMenuItem<String>(
              value: dept['dep_id'].toString(),
              child: Text(dept['dep_nombre'].toString()),
            );
          }).toList(),
          decoration: InputDecoration(
            labelText: 'Selecciona tu departamento',
            border: OutlineInputBorder(),
          ),
          value: _selectedDepartment,
          onChanged: (String? newValue) {
            setState(() {
              _selectedDepartment = newValue;
            });
          },
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Por favor, selecciona un departamento';
            }
            return null;
          },
        ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Provincia', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Municipio', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Zona', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                DropdownButtonFormField<String>(
  items: vias.map((via) {
    return DropdownMenuItem<String>(
      value: via['via_id'].toString(),
      child: Text(via['via_nombre'].toString()),
    );
  }).toList(),
  decoration: InputDecoration(
    labelText: 'Selecciona Tipo de vía',
    border: OutlineInputBorder(),
  ),
  value: _selectedVia,
  onChanged: (String? newValue) {
    setState(() {
      _selectedVia = newValue;
    });
  },
  validator: (value) {
    if (value == null || value.isEmpty) {
      return 'Por favor, selecciona un tipo de vía';
    }
    return null;
  },
),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Nombre Vía', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Nro. de Vivienda',
                      border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Edificio', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Bloque', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Piso', border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Nro. de Departamento',
                      border: OutlineInputBorder()),
                  validator: (value) {
                    // Puedes agregar validaciones personalizadas para la fecha de nacimiento
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      // Si todos los campos son válidos, puedes realizar alguna acción
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text('Datos válidos')),
                      );
                    }
                  },
                  child: Text('Guardar'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
