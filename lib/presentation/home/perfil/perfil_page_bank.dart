/// Dependencias de Flutter
import 'dart:math';

/// Dependencias de la app
import 'package:flutter/material.dart';
import 'package:igob_plus/presentation/home/perfil/carnet_page.dart';
import 'package:igob_plus/presentation/home/perfil/cuenta_page.dart';
import 'package:igob_plus/presentation/home/perfil/direccion_page.dart';

/// Dependencias de  https://pub.dev/

// data source
Map userData = {
  'name': 'Ricardo Juan Rey',
  'avatar':
      "http://34.72.103.248:3000/images/media_library/1699970782589-907629759.png",
};

class PageProfileBank extends StatelessWidget {
  const PageProfileBank({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ajustes'),
      ),
      body: body(context),
    );
  }

  Widget body(BuildContext context) {
    // style
    TextStyle categoryTitleStyle =
        const TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500);
    return ListView(
      children: <Widget>[
        // view : perfil
        Padding(
          padding: const EdgeInsets.only(
              top: 0.0, bottom: 20.0, left: 20.0, right: 20.0),
          child: viewProfile(),
        ),
        const SizedBox(height: 12.0),
        item(
            context: context,
            title: "Cuenta",
            descripcion: 'Nombre,  nro carnet y fecha de nacimiento',
            icon: Icons.person,
            fillColor: true),
        item(
            context: context,
            title: "Dirección",
            descripcion: 'Departamento, provincia y Municipio',
            icon: Icons.location_on,
            fillColor: true),
        item(
            context: context,
            title: "Carnet",
            descripcion: 'Fecha de vencimiento, foto anverso y reverso',
            icon: Icons.credit_card,
            fillColor: true),
        // text : category help
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          child: Text("Ayuda", style: categoryTitleStyle),
        ),
        item(
            context: context,
            title: "Centro de ayuda",
            icon: Icons.help_outline),
        item(
            context: context,
            title: "Preguntas frecuentes",
            icon: Icons.question_answer),
        item(context: context, title: "Contáctanos", icon: Icons.phone),
      ],
    );
  }

  Widget viewProfile() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 75,
          height: 75,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Colors.blue, width: 2),
          ),
          child: Center(
            child: ClipOval(
              child: FadeInImage.assetNetwork(
                placeholder:
                    'assets/images/loading.gif', // Imagen de carga mientras se carga la imagen real
                image:
                    'http://34.72.103.248:3000/images/media_library/1699970782589-907629759.png',
                fit: BoxFit.cover,
                width: 75,
                height: 75,
                imageErrorBuilder: (context, error, stackTrace) {
                  return Icon(Icons
                      .error); // Manejo de error si la imagen no se puede cargar
                },
              ),
            ),
          ),
        ),
        const SizedBox(width: 10.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // text : nombre del usuario
            Text(userData['name'],
                style: const TextStyle(fontWeight: FontWeight.w500)),
            // button : cerrar sesion
            GestureDetector(
              onTap: () {},
              child: const Text('Cerrar sesión',
                  style: TextStyle(color: Colors.blue)),
            ),
          ],
        ),
      ],
    );
  }

  /// WIDGETS COMPONENT
  Widget buttonCircle(
      {required String texto,
      required IconData icon,
      Color color = Colors.black}) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: 60.0,
            height: 60.0,
            child: RawMaterialButton(
                onPressed: () {},
                elevation: 1.0,
                fillColor: Colors.white,
                padding: const EdgeInsets.all(15.0),
                shape: const CircleBorder(),
                child: Icon(
                  icon,
                  size: 30.0,
                  color: color,
                )),
          ),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(texto,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: color,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold))),
        ],
      ),
    );
  }

  Widget item(
      {required BuildContext context,
      bool fillColor = false,
      IconData icon = Icons.attach_money,
      required String title,
      String descripcion = ""}) {
    // var
    bool darkMode = Theme.of(context).brightness == Brightness.dark;
    // style
    Color iconColor = darkMode ? Colors.white : Colors.black;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ListTile(
          onTap: () {
            if (title == "Cuenta") {
              print('Cuenta');
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => CuentaPage()),
              );
            } else if (title == "Dirección") {
              print('Dirección');
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => DireccionPage()),
              );
            } else if (title == "Carnet") {
              print('Carnet');
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => CarnetPage()),
              );
            }
          },
          leading: Container(
              width: 30.0,
              height: 30.0,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: fillColor
                      ? iconColor.withOpacity(0.15)
                      : Colors.transparent),
              child: Icon(icon, size: 20.0, color: iconColor)),
          title: Text(title, style: const TextStyle(fontSize: 18.0)),
          dense: false,
          subtitle: descripcion != "" ? Text(descripcion) : null,
          trailing: const Icon(Icons.keyboard_arrow_right),
        ),
        const Divider(height: 0.0, thickness: 0.3, indent: 20, endIndent: 30),
      ],
    );
  }

  Widget bottomNavigationBaar(
      {required BuildContext context,
      MaterialColor colorAccent = Colors.blue}) {
    // style
    Color backgroundColor = Theme.of(context).scaffoldBackgroundColor;

    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: backgroundColor,
        primaryColor: colorAccent,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Divider(height: 0.0, thickness: 0.3, indent: 0, endIndent: 0),
          BottomNavigationBar(
            elevation: 10.0,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(Icons.home, size: 24.0), label: ""),
              BottomNavigationBarItem(
                  icon: Icon(Icons.credit_card, size: 24.0), label: ""),
              BottomNavigationBarItem(
                  icon: Icon(Icons.account_balance_wallet, size: 24.0),
                  label: ""),
              BottomNavigationBarItem(
                  icon: Icon(Icons.assessment, size: 24.0), label: ""),
              BottomNavigationBarItem(
                  icon: Icon(Icons.perm_contact_calendar, size: 24.0),
                  label: ""),
            ],
            currentIndex: 4,
            selectedItemColor: colorAccent,
            unselectedItemColor: colorAccent[100],
          ),
        ],
      ),
    );
  }
}
