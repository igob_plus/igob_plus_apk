//Aquí está tu pantalla principal, pero ahora separada y usando el HomePresenter para la lógica.
import 'package:flutter/material.dart';
import 'package:igob_plus/presentation/home/cita_medica/cita_medica_page.dart';
import 'package:igob_plus/presentation/home/perfil/perfil_page_bank.dart';
import 'package:igob_plus/presentation/home/sitram/sitram_page.dart';



import 'home_presenter.dart';
import 'package:igob_plus/core/widgets/inputs_components.dart';
import 'package:igob_plus/core/widgets/cards_componentes.dart';
import 'package:igob_plus/core/widgets/divisor_componente.dart';
import 'package:igob_plus/core/widgets/botones_componentes.dart';
//import 'package:igob_plus/presentation/home/home_presenter.dart';
//revisar esta partte para no usar igob_plus

class MyHomePage extends StatefulWidget {
  final HomePresenter presenter;

  MyHomePage(this.presenter);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int count = 0;
  String result = "";
  TextEditingController versionController = TextEditingController();

  // Controladores para los campos de texto personalizados
  TextEditingController numberController = TextEditingController();
  TextEditingController letterController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController pruebaControler = TextEditingController();
  TextEditingController pruebaControler2 = TextEditingController();

  void _fetchData() async {
    try {
      final response = await widget.presenter.fetchData(versionController.text);
      setState(() {
        result = response;
      });
    } catch (e) {
      setState(() {
        result = "Error al conectar al servicio web";
      });
    }
  }

  //aqui iba otra funcion
  @override
  void dispose() {
    // Desechar los controladores cuando ya no se necesiten
    versionController.dispose();
    numberController.dispose();
    letterController.dispose();
    emailController.dispose();
    pruebaControler.dispose();
    pruebaControler2.dispose();

    super.dispose();
  }

//
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Material Design 3'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Número de toques: $count',
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  count++;
                });
              },
              child: Text('+'),
            ),
            ElevatedButton(
              onPressed: () {
                print('Perfil');
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          PageProfileBank()), // Abrir SitramPage
                );
              },
              child: Text('Perfil'),
            ),
            ElevatedButton(
              onPressed: () {
                print('Sitram');
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          SitramPage(widget.presenter)), // Abrir SitramPage
                );
              },
              child: Text('Sitram'),
            ),
            ElevatedButton(
              onPressed: () {
                print('citaMedica');
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          CitaMedicaPage(widget.presenter)), // Abrir SitramPage
                );
              },
              child: Text('Cita Médica'),
            ),
            TextField(
              controller: versionController,
              decoration: InputDecoration(labelText: 'Versión'),
            ),
            //
//inputs
            NumberInputField(
              controller: numberController,
              labelText: 'Número',
            ),
            SizedBox(height: 20),
            LetterInputField(
              controller: letterController,
              labelText: 'Letras',
            ),
            SizedBox(height: 20),
            EmailInputField(
              controller: emailController,
              labelText: 'Email',
            ),

// cards
            usuarioInputField(
                controller: pruebaControler2, labelText: "tumama"),
            SizedBox(
              height: 30,
            ),
            Cards(),
            SizedBox(
              height: 30,
            ),
            // Aquí se agrega el widget Cards
            Dividers(),
            SizedBox(
              height: 30,
            ),
            RegisterButton(
              onPressed: _suma,
              buttonText: 'sumar',
            ),
            SizedBox(
              height: 30,
            ),
            usuarioInputField(controller: pruebaControler, labelText: ""),
            SizedBox(
              height: 30,
            ),
            usuarioInputField2(
                controller: pruebaControler, labelText: "salida"),
//
            ElevatedButton(
              onPressed: _fetchData,
              child: Text('Conectar servicio web'),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Respuesta del servicio web:',
              style: TextStyle(fontSize: 16),
            ),
            Text(
              result,
              style: TextStyle(fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }

  void _suma() async {
    try {
      //  final response = await widget.presenter.sumaIncremental(1 as String);
      setState(() {
        count++;
      });
    } catch (e) {
      setState(() {
        count = "error en la suma" as int;
      });
    }
  }
}
