//En esta sección, vamos a manejar la lógica detrás de la presentación, o sea, todo lo que tiene que ver con cómo funciona la interfaz de usuario.
// presentation/home/home_presenter.dart

import 'package:igob_plus/core/uses_cases/fetch_data_use_case.dart';
import 'package:igob_plus/infrastructure/models/cita_medica/all_atenciones.dart';
import 'package:igob_plus/infrastructure/models/cita_medica/all_hospitales.dart';
import 'package:igob_plus/infrastructure/models/cita_medica/all_rechazos.dart';

import '../../infrastructure/models/sigue_tu_tramite/all_tramites.dart';
import '../../infrastructure/models/sigue_tu_tramite/pasos_tramite.dart';

class HomePresenter {
  final FetchDataUseCase fetchDataUseCase;

  HomePresenter(this.fetchDataUseCase);

  Future<String> fetchData(String version) async {
    try {
      final result = await fetchDataUseCase.execute(version);
      return result.toString();
    } catch (error) {
      return "Error al conectar al servicio web";
    }
  }

  Future<List<Tramite>> getAllTramites() async {
    try {
      final result = await fetchDataUseCase.getAllTramites();
      //print(result);
      //print('********************');
      if (result.containsKey('success') &&
          result['success'] is Map<String, dynamic>) {
        //print('En el if');
        //print(result.success);
        final successMap = result['success'] as Map<String, dynamic>;
        //print(successMap);

        if (successMap.containsKey('data')) {
          //print(successMap['data']);
          final data = successMap['data'];
          //print("size: "+data.isEmpty);
          // Aquí puedes trabajar con la variable 'data' obtenida
          List<Tramite> listaTramites =
              allTramitesFromJson(data); // Esta es tu lista de tramites
          List<Tramite> listaOrdenada =
              ordenarTrmitePorFecha(listaTramites); // Lista ordenada por fecha
          return listaOrdenada;
        } else {
          throw Exception("'data' no existe en 'success'");
        }
      } else {
        throw Exception("'success' no es un mapa o no existe en el resultado");
      }
    } catch (error) {
      print(error);
      throw Exception("Error al conectar al servicio webbb");
      //return {"error": "Error al conectar al servicio web"};;
    }
  }

  Future<List<PasoTramite>> getAllPasosTramite(
      int nroTramite, String clave) async {
    try {
      final result =
          await fetchDataUseCase.getAllPasosTramite(nroTramite, clave);
      if (result.containsKey('success') &&
          result['success'] is Map<String, dynamic>) {
        //print(result.success);
        final successMap = result['success'] as Map<String, dynamic>;

        if (successMap.containsKey('data')) {
          final data = successMap['data'];
          print(data);
          //print("size: "+data.isEmpty);
          // Aquí puedes trabajar con la variable 'data' obtenida

          return allPasosTramiteFromJson(data);
        } else {
          throw Exception("'data' no existe en 'success'");
        }
      } else {
        throw Exception("'success' no es un mapa o no existe en el resultado");
      }
    } catch (error) {
      //print(error);
      throw Exception("Error al conectar al servicio webbbsdsd");
      //return {"error": "Error al conectar al servicio web"};;
    }
  }

  Future<List<Rechazo>> getAllRechazos() async {
    try {
      final result = await fetchDataUseCase.getAllRechazos();
      if (result.containsKey('success') &&
          result['success'] is Map<String, dynamic>) {
        final successMap = result['success'] as Map<String, dynamic>;
        if (successMap.containsKey('data')) {
          final dataList = successMap['data'] as List<dynamic>;
          final data = dataList[0]['sp_dinamico'];
          return allRechazosFromJson(data);
        } else {
          throw Exception("'data' no existe en 'success'");
        }
      } else {
        throw Exception("'success' no es un mapa o no existe en el resultado");
      }
    } catch (error) {
      //print(error);
      throw Exception("Error al conectar al servicio webbbsdsd");
      //return {"error": "Error al conectar al servicio web"};;
    }
  }

  Future<List<Hospital>> getAllHospitales() async {
    try {
      final result = await fetchDataUseCase.getAllHospitales();
      if (result.containsKey('success') &&
          result['success'] is Map<String, dynamic>) {
        final successMap = result['success'] as Map<String, dynamic>;

        if (successMap.containsKey('data')) {
          return allHospitalesFromJson(successMap['data']);
        } else {
          throw Exception("'data' no existe en 'success'");
        }
      } else {
        throw Exception("'success' no es un mapa o no existe en el resultado");
      }
    } catch (error) {
      print(error);
      throw Exception("Error al conectar al servicio webbbsdsd");
      //return {"error": "Error al conectar al servicio web"};;
    }
  }

  Future<List<Atencion>> getAllAtenciones() async {
    try {
      final result = await fetchDataUseCase.getAllAtenciones();
      if (result.containsKey('success') &&
          result['success'] is Map<String, dynamic>) {
        final successMap = result['success'] as Map<String, dynamic>;

        if (successMap.containsKey('data')) {
          return allAtencionesFromJson(successMap['data']);
        } else {
          throw Exception("'data' no existe en 'success'");
        }
      } else {
        throw Exception("'success' no es un mapa o no existe en el resultado");
      }
    } catch (error) {
      print(error);
      throw Exception("Error al conectar al servicio webbbsdsd");
      //return {"error": "Error al conectar al servicio web"};;
    }
  }
}
