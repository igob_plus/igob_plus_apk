//Aquí está tu pantalla principal, pero ahora separada y usando el HomePresenter para la lógica.
import 'package:flutter/material.dart';
import 'package:igob_plus/infrastructure/models/cita_medica/all_hospitales.dart';
import 'package:igob_plus/presentation/home/home_presenter.dart';

class ReservasScreen extends StatefulWidget {
  final HomePresenter presenter; // El presentador para la pantalla de Reservas

  const ReservasScreen({Key? key, required this.presenter}) : super(key: key);

  @override
  _ReservasScreenState createState() => _ReservasScreenState();
}

class _ReservasScreenState extends State<ReservasScreen> {
  List<Hospital> items = [];
  bool isLoading = true;
  bool hasError = false;

  @override
  void initState() {
    super.initState();

    _refreshList();
  }

  void _setLoading(bool loading) {
    setState(() {
      isLoading = loading;
      hasError = false;
    });
  }

  Future<void> _refreshList() async {
    _setLoading(true);
    try {

      await widget.presenter.getAllAtenciones();
      final response = await widget.presenter.getAllHospitales();
      final dataList = response;
      setState(() {
        items = dataList;
      });
      _setLoading(false);
    } catch (e) {
      _setLoading(false);
      setState(() {
        hasError = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Text(
            'En esta seccion puedes ver la lista de Hospitales donde puede realizar una reserva.',
          ),
        ),
        if (isLoading)
          Center(child: CircularProgressIndicator())
        else if (hasError)
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Problemas de conexión'),
                SizedBox(height: 16),
                ElevatedButton(
                  onPressed: _refreshList,
                  child: Text('Recargar'),
                ),
              ],
            ),
          )
        else if (items.length == 0)
          Padding(
              padding: EdgeInsets.all(32.0),
              child: Card(
                color: Colors.blue, // Cambia el color de fondo del Card a azul
                child: Padding(
                  padding: EdgeInsets.all(
                      16.0), // Añade un padding de 16 en todas las direcciones
                  child: Text(
                    '¡Estimado ciudadano, no contamos con hospitales registrados a la fecha!',
                    style: TextStyle(
                        color:
                            Colors.white), // Cambia el color del texto a blanco
                  ),
                ),
              ))
        else
          Expanded(
            child: RefreshIndicator(
              onRefresh: _refreshList,
              child: ListView.builder(
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      ListTile(
                        leading: Container(
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // Icono de hospital
                              Image.asset(
                                'assets/icons/hospitalficha.png',
                                width: 50,
                                height: 50,
                              ),
                              SizedBox(
                                  height:
                                      4), // Espaciado entre el icono y el texto
                              // Texto en la parte inferior
                            ],
                          ),
                        ),
                        title: Text(
                          items[index].vhsp_nombre_hospital,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(items[index].vhsp_direccion),
                        onTap: () {
                          print('click');
                          _mostrarBottomSheet(context, items[index]);
                        },
                      ),
                      Divider(
                        color: Colors.grey,
                        thickness: 1.0,
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
      ],
    );
  }

  void _mostrarBottomSheet(BuildContext context, Hospital hospital) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              // Imagen

              // Título
              Text(
                hospital.vhsp_nombre_hospital,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 5),
              // Subtítulo
              Text(
                hospital.vhsp_descripcion,
                style: TextStyle(
                  fontSize: 14,
                  fontStyle: FontStyle.italic,
                ),
              ),
              SizedBox(height: 10),
              // Dirección
              Row(
                children: [
                  Icon(Icons.location_on),
                  SizedBox(width: 5),
                  Expanded(
                    child: Container(
                      constraints: BoxConstraints(maxWidth: double.infinity),
                      child: Text(hospital.vhsp_direccion,
                          overflow: TextOverflow.visible,
                          style: TextStyle(fontSize: 14)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              // Teléfono
              Row(
                children: [
                  Icon(Icons.phone),
                  SizedBox(width: 5),
                  Text(hospital.vhsp_telefono),
                ],
              ),
              SizedBox(height: 10),
              // Otros iconos
              Row(
                children: [
                  Icon(Icons.email),
                  SizedBox(width: 5),
                  Icon(Icons.web),
                  SizedBox(width: 5),
                  Icon(Icons.info),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
