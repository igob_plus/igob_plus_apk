//Aquí está tu pantalla principal, pero ahora separada y usando el HomePresenter para la lógica.
import 'package:flutter/material.dart';
import 'package:igob_plus/infrastructure/models/cita_medica/all_atenciones.dart';
import 'package:igob_plus/infrastructure/models/cita_medica/all_hospitales.dart';
import 'package:igob_plus/presentation/home/home_presenter.dart';

class AtencionScreen extends StatefulWidget {
  final HomePresenter presenter; // El presentador para la pantalla de Atencion

  const AtencionScreen({Key? key, required this.presenter}) : super(key: key);

  @override
  _AtencionScreenState createState() => _AtencionScreenState();
}

class _AtencionScreenState extends State<AtencionScreen> {
  List<Hospital> hospitales = [];
  List<Atencion> historial = [];
  List<Atencion> historial_filtrado = [];
  String nombreHospital = "";
  bool isLoading = true;
  bool hasError = false;

  Hospital? _selectedHospital;

  @override
  void initState() {
    super.initState();

    _refreshHospitales();
  }

  void _setLoading(bool loading) {
    setState(() {
      isLoading = loading;
      hasError = false;
    });
  }

  void _filtrarHistorial(String vhsp_nombre_hospital) {
    print('Filtramso de:' + vhsp_nombre_hospital);
    final historial_filtrado_aux = historial
        .where(
            (element) => element.vhsp_nombre_hospital == vhsp_nombre_hospital)
        .toList();

    setState(() {
      historial_filtrado = historial_filtrado_aux;
      nombreHospital = vhsp_nombre_hospital;
    });
  }

  Future<void> _refreshHospitales() async {
    
    _setLoading(true);
    try {
      //preguntamos los hospitales
      final response = await widget.presenter.getAllHospitales();
      final itemsConVexisteUno =
          response.where((element) => element.vexiste == 1).toList();
      setState(() {
        hospitales = itemsConVexisteUno;
      });
      //Preguntamos el historial de reervas
      final response_atenciones = await widget.presenter.getAllAtenciones();
      setState(() {
        historial = response_atenciones;
      });
      _setLoading(false);
    } catch (e) {
      _setLoading(false);
      setState(() {
        hasError = true;
      });
    }
  }
Future<void> _refreshHospitales2() async {
  print('Actualizamos lista');
}
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Text(
            'En esta seccion puedes ver tu historial de Atenciones por hospital.',
          ),
        ),
        
        if (isLoading)
          Center(child: CircularProgressIndicator())
        else if (hasError)
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Problemas de conexión'),
                SizedBox(height: 16),
                ElevatedButton(
                  onPressed: _refreshHospitales,
                  child: Text('Recargar'),
                ),
              ],
            ),
          )
        else if (hospitales.length == 0)
          Padding(
              padding: EdgeInsets.all(32.0),
              child: Card(
                color: Colors.blue, // Cambia el color de fondo del Card a azul
                child: Padding(
                  padding: EdgeInsets.all(
                      16.0), // Añade un padding de 16 en todas las direcciones
                  child: Text(
                    '¡Estimado ciudadano, no tienes un historial medico en ningun hospital registrado!',
                    style: TextStyle(
                        color:
                            Colors.white), // Cambia el color del texto a blanco
                  ),
                ),
              ))
        else if(hospitales.length != 0)
        Center(
                child: DropdownButton<Hospital>(
                  hint: Text('Selecciona un hospital'),
                  value:
                      _selectedHospital, // Valor seleccionado (null para el ejemplo)
                  icon: Icon(
                    Icons.arrow_drop_down,
                    color: Colors.blue, // Color del icono del botón desplegable
                  ),
                  onChanged: (Hospital? newValue) {
                    // Manejar el cambio de selección
                    setState(() {
                      _selectedHospital = newValue;
                      // Aquí puedes realizar otras acciones al seleccionar un nuevo hospital
                    });
                    _filtrarHistorial(newValue!.vhsp_nombre_hospital);
                    //print('Hospital seleccionado: ${newValue!.vhsp_nombre_hospital}');
                  },
                  items: hospitales.map<DropdownMenuItem<Hospital>>(
                    (Hospital hospital) {
                      return DropdownMenuItem<Hospital>(
                        value: hospital,
                        child: Text(hospital.vhsp_nombre_hospital),
                      );
                    },
                  ).toList(),
                ),
              ),
        
        if(nombreHospital!='')
          
              
              Expanded(
                child: RefreshIndicator(
                  onRefresh: _refreshHospitales2,
                  child: ListView.builder(
                    itemCount: historial_filtrado.length,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          ListTile(
                            leading: Container(
                              // Estilo del contenedor del avatar
                              width: 70,
                              height: 60,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    historial_filtrado[index].vhcl_codigoseg,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    historial_filtrado[index]
                                        .presfecha_atencion,
                                    style: TextStyle(
                                      fontSize: 10,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            title: Text(
                              historial_filtrado[index].vhsp_nombre_hospital,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(
                                historial_filtrado[index].espdesc_especialidad),
                            onTap: () {
                              print('click');
                              _mostrarBottomSheet(
                                  context, historial_filtrado[index]);
                            },
                          ),
                          Divider(
                            color: Colors.grey,
                            thickness: 1.0,
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
           
      ],
    );
  }

  void _mostrarBottomSheet(BuildContext context, Atencion Atencion) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              // Imagen

              // Título
              Text(
                Atencion.vhsp_nombre_hospital,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 5),
              // Subtítulo
              Text(
                Atencion.cnsldescripcion,
                style: TextStyle(
                  fontSize: 14,
                  fontStyle: FontStyle.italic,
                ),
              ),
              SizedBox(height: 10),
              // Dirección
              Row(
                children: [
                  Icon(Icons.location_on),
                  SizedBox(width: 5),
                  Expanded(
                    child: Container(
                      constraints: BoxConstraints(maxWidth: double.infinity),
                      child: Text(Atencion.vhsp_direccion,
                          overflow: TextOverflow.visible,
                          style: TextStyle(fontSize: 14)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              // Teléfono
              Row(
                children: [
                  Icon(Icons.phone),
                  SizedBox(width: 5),
                  Text(Atencion.vhsp_telefono),
                ],
              ),
              SizedBox(height: 10),
              // Otros iconos
              Row(
                children: [
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        // Aquí puedes agregar la lógica para reservar la ficha
                        // Por ejemplo, podrías navegar a otra pantalla o realizar una acción específica al presionar el botón
                        // También podrías llamar a una función para iniciar el proceso de reserva
                        // Coloca aquí tu lógica de reserva
                      },
                      child: Text('Reservar Ficha'),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
