//Aquí está tu pantalla principal, pero ahora separada y usando el HomePresenter para la lógica.
import 'package:flutter/material.dart';
import 'package:igob_plus/presentation/home/cita_medica/atenciones_screem.dart';
import 'package:igob_plus/presentation/home/cita_medica/historial_screem.dart';
import 'package:igob_plus/presentation/home/cita_medica/rechazo_screem.dart';
import 'package:igob_plus/presentation/home/cita_medica/reservas_screem.dart';
import '../home_presenter.dart';

class CitaMedicaPage extends StatefulWidget {
  final HomePresenter presenter;

  CitaMedicaPage(this.presenter);

  @override
  _CitaMedicaPageState createState() => _CitaMedicaPageState();
}

class _CitaMedicaPageState extends State<CitaMedicaPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Salud - Cita Médica'),
      ),
      body: Column(
        children: <Widget>[
          Material(
            color: Colors.white, // Cambiar el color del TabBar
            elevation: 4, // Añadir elevación para resaltar el TabBar
            child: TabBar(
              controller: _tabController,
              tabs: [
                Tab(
                  icon: Icon(Icons.event),
                  text: 'RESERVAS',
                ),
                Tab(
                  icon: Icon(Icons.history),
                  text: 'HISTORIAL',
                ),
                Tab(
                  icon: Icon(Icons.local_hospital),
                  text: 'ATENCIONES',
                ),
                Tab(
                  icon: Icon(Icons.cancel),
                  text: 'RECHAZO',
                ),
              ],
              labelStyle:
                  TextStyle(fontSize: 12.0), // Ajustar el tamaño del texto
              labelPadding: EdgeInsets.symmetric(
                  vertical:
                      8.0), // Ajustar el espacio alrededor del texto e icono
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [
                ReservasScreen(presenter: widget.presenter),
                HistorialScreen(presenter: widget.presenter),
                AtencionScreen(presenter: widget.presenter),
                RechazoScreen(presenter: widget.presenter),
              ],
            ),
          ),
        ],
      ),
      //backgroundColor: Colors.blue, // Cambiar el color de fondo del Scaffold
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}
