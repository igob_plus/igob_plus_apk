//Aquí está tu pantalla principal, pero ahora separada y usando el HomePresenter para la lógica.
import 'package:flutter/material.dart';
import 'package:igob_plus/infrastructure/models/cita_medica/all_atenciones.dart';
import 'package:igob_plus/presentation/home/home_presenter.dart';

class HistorialScreen extends StatefulWidget {
  final HomePresenter presenter; // El presentador para la pantalla de Historial

  const HistorialScreen({Key? key, required this.presenter}) : super(key: key);

  @override
  _HistorialScreenState createState() => _HistorialScreenState();
}

class _HistorialScreenState extends State<HistorialScreen> {
  List<Atencion> items = [];
  bool isLoading = true;
  bool hasError = false;

  @override
  void initState() {
    super.initState();

    _refreshList();
  }

  void _setLoading(bool loading) {
    setState(() {
      isLoading = loading;
      hasError = false;
    });
  }

  Future<void> _refreshList() async {
    _setLoading(true);
    try {
      final response = await widget.presenter.getAllAtenciones();
      final dataList = response;
      setState(() {
        items = dataList;
      });
      _setLoading(false);
    } catch (e) {
      _setLoading(false);
      setState(() {
        hasError = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Text(
            'En esta seccion puedes ver su historial de Atenciones medicas.',
          ),
        ),
        if (isLoading)
          Center(child: CircularProgressIndicator())
        else if (hasError)
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Problemas de conexión'),
                SizedBox(height: 16),
                ElevatedButton(
                  onPressed: _refreshList,
                  child: Text('Recargar'),
                ),
              ],
            ),
          )
        else if (items.length == 0)
          Padding(
              padding: EdgeInsets.all(32.0),
              child: Card(
                color: Colors.blue, // Cambia el color de fondo del Card a azul
                child: Padding(
                  padding: EdgeInsets.all(
                      16.0), // Añade un padding de 16 en todas las direcciones
                  child: Text(
                    '¡Estimado ciudadano, no contamos con Atenciones registrados a la fecha!',
                    style: TextStyle(
                        color:
                            Colors.white), // Cambia el color del texto a blanco
                  ),
                ),
              ))
        else
          Expanded(
            child: RefreshIndicator(
              onRefresh: _refreshList,
              child: ListView.builder(
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      ListTile(
                        leading: Container(
                          // Estilo del contenedor del avatar
                          width: 70,
                          height: 60,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                items[index].vhcl_codigoseg,
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                items[index].presfecha_atencion,
                                style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                        title: Text(
                          items[index].vhsp_nombre_hospital,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(items[index].espdesc_especialidad),
                        onTap: () {
                          print('click');
                          _mostrarBottomSheet(context, items[index]);
                        },
                      ),
                      Divider(
                        color: Colors.grey,
                        thickness: 1.0,
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
      ],
    );
  }

  void _mostrarBottomSheet(BuildContext context, Atencion Atencion) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              // Imagen

              // Título
              Text(
                Atencion.vhsp_nombre_hospital,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 5),
              // Subtítulo
              Text(
                Atencion.cnsldescripcion,
                style: TextStyle(
                  fontSize: 14,
                  fontStyle: FontStyle.italic,
                ),
              ),
              SizedBox(height: 10),
              // Dirección
              Row(
                children: [
                  Icon(Icons.location_on),
                  SizedBox(width: 5),
                  Expanded(
                    child: Container(
                      constraints: BoxConstraints(maxWidth: double.infinity),
                      child: Text(Atencion.vhsp_direccion,
                          overflow: TextOverflow.visible,
                          style: TextStyle(fontSize: 14)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              // Dirección
              Row(
                children: [
                  Icon(Icons.medical_services),
                  SizedBox(width: 5),
                  Expanded(
                    child: Container(
                      constraints: BoxConstraints(maxWidth: double.infinity),
                      child: Text(Atencion.paciente_nombre,
                          overflow: TextOverflow.visible,
                          style: TextStyle(fontSize: 14)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              // Teléfono
              Row(
                children: [
                  Icon(Icons.phone),
                  SizedBox(width: 5),
                  Text(Atencion.vhsp_telefono),
                ],
              ),
              SizedBox(height: 10),
              // Otros iconos
              Row(
                children: [
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        // Aquí puedes agregar la lógica para reservar la ficha
                        // Por ejemplo, podrías navegar a otra pantalla o realizar una acción específica al presionar el botón
                        // También podrías llamar a una función para iniciar el proceso de reserva
                        // Coloca aquí tu lógica de reserva
                      },
                      child: Text('Reservar Ficha'),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
