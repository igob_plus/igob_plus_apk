//Aquí está tu pantalla principal, pero ahora separada y usando el HomePresenter para la lógica.
import 'package:flutter/material.dart';
import 'package:igob_plus/infrastructure/models/cita_medica/all_rechazos.dart';
import 'package:igob_plus/presentation/home/home_presenter.dart';

class RechazoScreen extends StatefulWidget {
  final HomePresenter presenter; // El presentador para la pantalla de Rechazo

  const RechazoScreen({Key? key, required this.presenter}) : super(key: key);

  @override
  _RechazoScreenState createState() => _RechazoScreenState();
}

class _RechazoScreenState extends State<RechazoScreen> {
  List<Rechazo> items = [];
  bool isLoading = true;
  bool hasError = false;

  @override
  void initState() {
    super.initState();

    _refreshList();
  }

  void _setLoading(bool loading) {
    setState(() {
      isLoading = loading;
      hasError = false;
    });
  }

  Future<void> _refreshList() async {
    _setLoading(true);
    try {
      //await widget.presenter.getAllHospitales();
      final response = await widget.presenter.getAllRechazos();
      final dataList = response;
      setState(() {
        items = dataList;
      });
      _setLoading(false);
    } catch (e) {
      _setLoading(false);
      setState(() {
        hasError = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Text(
            'En esta seccion puedes ver la lista de reservas que fueron rechazas.',
          ),
        ),
        if (isLoading)
          Center(child: CircularProgressIndicator())
        else if (hasError)
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Problemas de conexión'),
                SizedBox(height: 16),
                ElevatedButton(
                  onPressed: _refreshList,
                  child: Text('Recargar'),
                ),
              ],
            ),
          )
        else if (items.length == 0)
          Padding(
              padding: EdgeInsets.all(32.0),
              child: Card(
                color: Colors.blue, // Cambia el color de fondo del Card a azul
                child: Padding(
                  padding: EdgeInsets.all(
                      16.0), // Añade un padding de 16 en todas las direcciones
                  child: Text(
                    '¡Estimado ciudadano, usted no cuenta con reservas Rechazadas a la fecha!',
                    style: TextStyle(
                        color:
                            Colors.white), // Cambia el color del texto a blanco
                  ),
                ),
              ))
        else
          Expanded(
            child: RefreshIndicator(
              onRefresh: _refreshList,
              child: ListView.builder(
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      ListTile(
                        leading: Container(
                            // Estilo del contenedor del avatar
                            width: 70,
                            height: 60,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: FadeInImage(
                              fit: BoxFit
                                  .cover, // Utiliza BoxFit.cover para ajustar al ancho
                              width: 70,
                              height: 60,
                              placeholder:
                                  const AssetImage('assets/images/loading.gif'),
                              image: NetworkImage(
                                items[index].vhjr_datos_referencia.URL_HOJA,
                              ),
                              imageErrorBuilder: (context, error, stackTrace) {
                                return Icon(Icons
                                    .error); // Mostrar un icono en caso de error al cargar la imagen
                              },
                            )),
                        title: Text(items[index].vhospital, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                        subtitle: Text(items[index].vhjr_motivo +
                            ' - ' +
                            items[index].vhjr_datos_referencia.FECHA_EMISION, style: TextStyle(fontSize: 12)),
                        onTap: () {
                          print('click');
                          _mostrarBottomSheet(context, items[index]);
                        },
                      ),
                      Divider(
                        color: Colors.grey,
                        thickness: 1.0,
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
      ],
    );
  }

   void _mostrarBottomSheet(BuildContext context, Rechazo rechazo) {
    showModalBottomSheet(
  context: context,
  builder: (BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.network(
              rechazo.vhjr_datos_referencia.URL_HOJA,
              fit: BoxFit.cover,
              height: MediaQuery.of(context).size.height * 0.8,
              width: MediaQuery.of(context).size.width,
            ),
            SizedBox(height: 16.0),
            // Otros widgets adicionales debajo de la imagen si es necesario
          ],
        ),
      ),
    );
  },
);

  }
}
