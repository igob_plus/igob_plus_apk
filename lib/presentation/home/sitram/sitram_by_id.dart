//Aquí está tu pantalla principal, pero ahora separada y usando el HomePresenter para la lógica.
import 'package:flutter/material.dart';
import 'package:igob_plus/core/widgets/inputs_components.dart';
import 'package:igob_plus/infrastructure/models/sigue_tu_tramite/all_tramites.dart';
import 'package:igob_plus/infrastructure/models/sigue_tu_tramite/pasos_tramite.dart';
import 'package:igob_plus/presentation/home/home_presenter.dart';


class SitramById extends StatefulWidget {
  final HomePresenter presenter;
  final int tcorrelativo;
  final String tipo_tramite;
  final String tfregistro;
  final String tiod;
  final String tasunto;
  SitramById(this.presenter, this.tcorrelativo, this.tipo_tramite,
      this.tfregistro, this.tiod, this.tasunto);

  @override
  _SitramById createState() => _SitramById();
}

class _SitramById extends State<SitramById> {
  List<PasoTramite> items = [];
  bool isLoading = true;
  bool hasError = false;
  @override
  void initState() {
    super.initState();
    _refreshList();
  }

  Success() {
    setState(() {
      this.isLoading = false;
      this.hasError = false;
    });
  }

  Error() {
    setState(() {
      this.isLoading = false;
      this.hasError = true;
    });
  }

  Loading() {
    if (items.isEmpty) {
      print('data:' + widget.tcorrelativo.toString());
      setState(() {
        this.isLoading = true;
        this.hasError = false;
      });
    }
  }

  Future<void> _refreshList() async {
    Loading();
    try {
      final response = await widget.presenter
          .getAllPasosTramite(widget.tcorrelativo, widget.tiod);
      final dataList = response;
      //print('Mostramos response');
      //print(dataList);
      setState(() {
        items = dataList;
      });
      Success();
    } catch (e) {
      print(e);
      Error();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Sigue tu tramite')),
      body: RefreshIndicator(
        onRefresh: _refreshList,
        child: ListView(
          children: [
            MyCardPasoTramite(
              tcorrelativo: widget.tcorrelativo,
              tipo_tramite: widget.tipo_tramite,
              tfregistro: widget.tfregistro,
              toid: widget.tiod,
              tasunto: widget.tasunto,
            ),
            isLoading
                ? Center(child: CircularProgressIndicator())
                : hasError
                    ? Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Problemas de conexión'),
                            SizedBox(height: 16),
                            ElevatedButton(
                              onPressed: () async {
                                await this._refreshList();
                              },
                              child: Text('Recargar'),
                            ),
                          ],
                        ),
                      )
                    : Container(
                        margin: EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: items
                              .asMap()
                              .entries
                              .map(
                                (entry) => _buildTimelineEntry(
                                  entry.value,
                                  entry.key + 1,
                                  items.length
                                ),
                              )
                              .toList(),
                        ),
                      ),
          ],
        ),
      ),
    );
  }
}

class MyCardPasoTramite extends StatelessWidget {
  final int tcorrelativo;
  final String tipo_tramite;
  final String tfregistro;
  final String toid;
  final String tasunto;

  MyCardPasoTramite({
    required this.tcorrelativo,
    required this.tipo_tramite,
    required this.tfregistro,
    required this.toid,
    required this.tasunto,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      
      elevation: 4.0, // Elevación del Card
  shape: RoundedRectangleBorder(
    side: BorderSide(
      color: Colors.blue, // Color del borde
      width: 2.0, // Ancho del borde
    ),
    borderRadius: BorderRadius.circular(12.0), // Borde del Card
  ),
      margin: EdgeInsets.all(16),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            
            Card(
              color: Colors.blue, // Cambia el color de fondo del Card a azul
              child: Padding(
                padding: EdgeInsets.all(
                    8.0), // Añade un padding de 16 en todas las direcciones
                child: Text(
                  '$tipo_tramite',
                  style: TextStyle(
                      color:
                          Colors.white), // Cambia el color del texto a blanco
                ),
              ),
            ),
            Text(
              '$tasunto',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 8),
            Text(
              'ID: $toid - $tfregistro',
              style: TextStyle(fontSize: 12),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _buildTimelineEntry(PasoTramite item, int index,int max) {
  return Padding(
    padding:
        EdgeInsets.all(0.0), // Elimina el padding de los elementos de la lista
    child: Stack(
      children: [
        Positioned(
          left: 40.0 / 2 - 2.5, // Centra la línea vertical detrás del círculo
          top: index != 1 ? 0.0 : 40.0 ,
          bottom: index != max ? 0.0 : 40.0,
          child: Container(
            width: 5.0,
            color: Colors.blue, // Color de la línea vertical
          ),
        ),
        Positioned(
          left: 0.0,
          top: 0.0,
          bottom: 0.0,
          child: Container(
            width: 40.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
  margin: EdgeInsets.symmetric(vertical: 8.0),
  decoration: BoxDecoration(
    shape: BoxShape.circle,
    border: Border.all(
      color: Colors.blue, // Cambiado a blanco
      width: 2.0,
    ),
  ),
  child: CircleAvatar(
    backgroundColor: max != index ? Colors.blue : Colors.white,
    child: Text(
      '$index',
      style: TextStyle(color: max != index ? Colors.white : Colors.blue),
    ),
  ),
),
              ],
            ),
          ),
        ),
        Padding(
          padding:
              EdgeInsets.only(left: 48.0), // Espacio a la derecha del círculo
          child: Container(
            padding: EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item.tno, // Título de la entrada
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(height: 8.0),
                Text(
                  item.trd, // Descripción de la entrada
                  style: TextStyle(fontSize: 14.0),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}
