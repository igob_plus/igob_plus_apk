//Aquí está tu pantalla principal, pero ahora separada y usando el HomePresenter para la lógica.
import 'package:flutter/material.dart';
import 'package:igob_plus/infrastructure/models/sigue_tu_tramite/all_tramites.dart';
import 'package:igob_plus/presentation/home/home_presenter.dart';
import 'package:igob_plus/presentation/home/sitram/sitram_by_id.dart';




class SitramPage extends StatefulWidget {
  final HomePresenter presenter;

  SitramPage(this.presenter);

  @override
  _SitramPageState createState() => _SitramPageState();
}

class _SitramPageState extends State<SitramPage> {
  List<Tramite> items = [];
  bool isLoading = true;
  bool hasError = false;

  @override
  void initState() {
    super.initState();
    _refreshList();
  }

  void _setLoading(bool loading) {
    setState(() {
      isLoading = loading;
      hasError = false;
    });
  }

  Future<void> _refreshList() async {
    _setLoading(true);
    try {
      final response = await widget.presenter.getAllTramites();
      final dataList = response;
      setState(() {
        items = dataList;
      });
      _setLoading(false);
    } catch (e) {
      _setLoading(false);
      setState(() {
        hasError = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Sigue tu trámite')),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(
              'Mediante este servicio podrá saber la ubicación y estado actual de su Trámite.',
            ),
          ),
          if (isLoading)
            Center(child: CircularProgressIndicator())
          else if (hasError)
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('Problemas de conexión'),
                  SizedBox(height: 16),
                  ElevatedButton(
                    onPressed: _refreshList,
                    child: Text('Recargar'),
                  ),
                ],
              ),
            )
          else if (items.length == 0)
            Padding(
                padding: EdgeInsets.all(32.0),
                child: Card(
                  color:
                      Colors.blue, // Cambia el color de fondo del Card a azul
                  child: Padding(
                    padding: EdgeInsets.all(
                        16.0), // Añade un padding de 16 en todas las direcciones
                    child: Text(
                      '¡Estimado ciudadano, usted no cuenta con trámites en GAMLP a la fecha!',
                      style: TextStyle(
                          color: Colors
                              .white), // Cambia el color del texto a blanco
                    ),
                  ),
                ))
          else
            Expanded(
              child: RefreshIndicator(
                onRefresh: _refreshList,
                child: ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        ListTile(
                          leading: Container(
                            // Estilo del contenedor del avatar
                            width: 70,
                            height: 60,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  items[index].toid,
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                ),
                                Text(
                                  items[index].tfregistro,
                                  style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          title: Text(items[index].tdata.tipo_tramite, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                          subtitle: Text(
                            items[index].tasunto.length > 55
                                ? '${items[index].tasunto.substring(0, 55)}...'
                                : items[index].tasunto,
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SitramById(
                                  widget.presenter,
                                  items[index].tcorrelativo,
                                  items[index].tdata.tipo_tramite,
                                  items[index].tfregistro,
                                  items[index].toid,
                                  items[index].tasunto,
                                ),
                              ),
                            );
                          },
                        ),
                        Divider(
                          color: Colors.grey,
                          thickness: 1.0,
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
        ],
      ),
    );
  }
}
