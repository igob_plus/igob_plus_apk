import 'package:flutter/material.dart';

class WelcomeScreen extends StatefulWidget {
  final Function onInitializationComplete;

  WelcomeScreen({required this.onInitializationComplete});

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  void initState() {
    super.initState();
    widget.onInitializationComplete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color(0xFF56BED5), Color(0xFFD264CA)],
          ),
        ),
        child: const Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "IGOB",
                    style: TextStyle(
                      fontFamily: "Comfortaa",
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0,
                      color: Colors.white,
                    ),
                  ),
                    Text(
                      "+",
                      style: TextStyle(
                        fontFamily: "Comfortaa",
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "plus",
                      style: TextStyle(
                        fontFamily: "Comfortaa",
                        fontSize: 20, // Tamaño de fuente para "plus"
                        fontWeight:
                            FontWeight.w700, // Peso de fuente para "plus"
                        letterSpacing: 0,
                        color: Colors.white,
                      ),
                    ),
                ],
              ),
             Text(
                  "experiencia renovada",
                  style: TextStyle(
                    fontFamily: "Comfortaa",
                    fontSize: 15,
                    fontWeight: FontWeight.normal,
                    letterSpacing: 0,
                    color: Colors.white,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
