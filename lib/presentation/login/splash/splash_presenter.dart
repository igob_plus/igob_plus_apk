import 'package:flutter/material.dart';
import 'package:igob_plus/core/uses_cases/fetch_data_use_case.dart';
import 'package:igob_plus/infrastructure/data_sources/api_client.dart';
import 'package:igob_plus/infrastructure/repositories/web_service_repository.dart';
import 'package:igob_plus/presentation/home/home_page.dart';
import 'package:igob_plus/presentation/home/home_presenter.dart';
import 'package:igob_plus/presentation/login/carrouselInicial/carrusel_page.dart';
import 'package:igob_plus/presentation/login/splash/splash_page.dart';

class SplashPresenter {
void navigateToHomePage(BuildContext context) {
  final apiClient = ApiClient();
  final webServiceRepository = WebServiceRepository(apiClient);
  final fetchDataUseCase = FetchDataUseCase(webServiceRepository);
  final homePresenter = HomePresenter(fetchDataUseCase);

  Future.delayed(Duration(milliseconds: 2000), () {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        //builder: (context) => MyHomePage(homePresenter),
        builder: (context) => OnBoardingPage(),
      ),
    );
  });
}

  Widget buildSplashScreen(BuildContext context) {
    return WelcomeScreen(
      onInitializationComplete: () => navigateToHomePage(context),
    );
  }
}
