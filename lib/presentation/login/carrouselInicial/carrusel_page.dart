import 'package:flutter/material.dart';
import 'carrusel_presenter.dart';
import 'package:igob_plus/presentation/home/home_page.dart';

class OnBoardingPage extends StatefulWidget {
  const OnBoardingPage({Key? key}) : super(key: key);

  @override
  State<OnBoardingPage> createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  late PageController _pageController;
  int _pageIndex = 0;

  @override
  void initState() {
    _pageController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return OnboardingPresenter(
      pageController: _pageController,
      pageIndex: _pageIndex,
      onPageChanged: (index) {
        setState(() {
          _pageIndex = index;
        });
      },
    );
  }
}
