import 'package:flutter/material.dart';
import 'package:igob_plus/core/uses_cases/fetch_data_use_case.dart';
import 'package:igob_plus/infrastructure/data_sources/api_client.dart';
import 'package:igob_plus/infrastructure/repositories/web_service_repository.dart';
import 'package:igob_plus/presentation/home/home_page.dart';
import 'package:igob_plus/presentation/home/home_presenter.dart';

class OnboardingPresenter extends StatelessWidget {
  final PageController pageController;
  final int pageIndex;
  final ValueChanged<int> onPageChanged;

  OnboardingPresenter({
    required this.pageController,
    required this.pageIndex,
    required this.onPageChanged,
  });

void _navigateToNextScreen(BuildContext context) {
  final apiClient = ApiClient();

 final webServiceRepository = WebServiceRepository(apiClient);
  final fetchDataUseCase = FetchDataUseCase(webServiceRepository);
  final homePresenter = HomePresenter(fetchDataUseCase);

  Navigator.of(context).pushReplacement(
    MaterialPageRoute(
             builder: (context) => MyHomePage(homePresenter),

    ),
  );
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Color(0xFFD166CA),
        ),
        actions: [
          if (pageIndex == 0)
            ElevatedButton(
              onPressed: () => _navigateToNextScreen(context),
              style: ElevatedButton.styleFrom(
                primary: Color(0xFFD166CA),
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 8.0,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24.0),
                ),
              ),
              child: const Row(
                children: [
                  Text(
                    'Omitir',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Expanded(
                child: PageView.builder(
                  itemCount: demo_data.length,
                  controller: pageController,
                  onPageChanged: onPageChanged,
                  itemBuilder: (context, index) {
                    return OnboardingContent(
                      images: demo_data[index].images,
                      title: demo_data[index].title,
                      description: demo_data[index].description,
                    );
                  },
                ),
              ),
              Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (pageIndex == demo_data.length - 1)
                        Positioned(
                          child: Container(
                            width: 250.0,
                            height: 43.0,
                            child: ElevatedButton(
                              onPressed: () => _navigateToNextScreen(context),
                              style: ElevatedButton.styleFrom(
                                primary: Color(0xFFD166CA),
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 45.0,
                                  vertical: 12.0,
                                ),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                              ),
                              child: const Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Empezar ahora',
                                    style: TextStyle(
                                      color: Color(0xFFFFFFFF),
                                      fontFamily: 'Lato',
                                      fontSize: 16,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w700,
                                      height: 1.0,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios_rounded,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      const SizedBox(
                        height: 16,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ...List.generate(
                            demo_data.length,
                            (index) => Padding(
                              padding: const EdgeInsets.only(right: 4),
                              child: DotIndicator(
                                isActive: index == pageIndex,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DotIndicator extends StatelessWidget {
  const DotIndicator({
    Key? key,
    this.isActive = false,
  }) : super(key: key);

  final bool isActive;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 10.0,
      width: 20.0,
      decoration: BoxDecoration(
        color: isActive ? Color(0xFFD166CA) : Color.fromARGB(255, 99, 98, 98),
        borderRadius: const BorderRadius.all(Radius.circular(7)),
      ),
    );
  }
}

class Onboard {
  final String images, title, description;
  Onboard({
    required this.images,
    required this.title,
    required this.description,
  });
}

final List<Onboard> demo_data = [
  Onboard(
    images: "assets/images/onbording_1.png",
    title: "!Bienvenido a iGOBPlus¡",
    description:
        "Una variedad de servicios que puedes seleccionar a tu gusto. Tú decides qué habilitar y deshabilitar.",
  ),
  Onboard(
    images: "assets/images/onbording_2.png",
    title: "Descubre una experiencia personalizada",
    description:
        "Elige tus servicios favoritos y personalízalos según tus necesidades",
  ),
  Onboard(
    images: "assets/images/onbording_3.png",
    title: "Configura tu identidad",
    description:
        "Crea un perfil con tu información básica, es rápido y sencillo.",
  ),
];

class OnboardingContent extends StatelessWidget {
  const OnboardingContent({
    Key? key,
    required this.images,
    required this.title,
    required this.description,
  }) : super(key: key);

  final String images, title, description;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Spacer(),
        Image.asset(
          images,
          height: 262,
        ),
        const SizedBox(
          height: 20,
        ),
        Text(
          title,
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: Color(0xFF303030),
            fontFamily: 'Lato',
            fontSize: 25.0,
            fontWeight: FontWeight.w800,
            height: 1.0,
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          description,
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: Color(0xFF9A9797),
            fontFamily: 'Lato',
            fontSize: 14.0,
            fontWeight: FontWeight.w400,
            height: 1.5,
          ),
        ),
        const Spacer(),
      ],
    );
  }
}
