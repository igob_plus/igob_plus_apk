
// inicio_page.dart

import 'package:flutter/material.dart';
import 'package:igob_plus/presentation/login/inicio/inicio_page.dart';
import 'inicio_presenter.dart';

class InicioPage extends StatefulWidget {
  @override
  _InicioPageState createState() => _InicioPageState();
}

class _InicioPageState extends State<InicioPage> {
  InicioPresenter _presenter = InicioPresenter();

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  void _loadData() {
    _presenter.fetchData();
    // Lógica para cargar datos en la página
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inicio'),
      ),
      body: Center(
        child: Text('Contenido de la página de inicio'),
      ),
    );
  }
}
